//
//  ShedulerVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit

class ShedulerVC: BaseVC {
    
    lazy private var fabricatorBtn = factory.fabricatorBtn
    lazy private var bayerBtn = factory.bayerBtn
    
    private var factory: ShedulerFactory
    
    init(_ factory: ShedulerFactory){
        self.factory = factory
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(fabricatorBtn)
        view.addSubview(bayerBtn)
    
        fabricatorBtn.addTarget(self, action: #selector(fabricatorPart), for: .touchUpInside)
        bayerBtn.addTarget(self, action: #selector(bayerBtnPart), for: .touchUpInside)
        
        fabricatorBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-32.0)
            make.height.equalTo(44.0)
            make.leading.trailing.equalToSuperview().inset(40.0)
        }
        bayerBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(32.0)
            make.height.equalTo(40.0)
            make.leading.trailing.equalToSuperview().inset(40.0)
        }
    }
    
    @objc func fabricatorPart(sender: UIButton){
        self.navigationController?.pushViewController(RegisterVC(.init()), animated: true)
    }
    @objc func bayerBtnPart(sender: UIButton){
        self.navigationController?.pushViewController(RegisterBayerVC(.init()), animated: true)
    }
}
