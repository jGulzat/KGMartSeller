//
//  LaunchVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 7/3/22.
//

import UIKit

class LaunchVC: BaseVC {
    
    lazy private var logoLaunch = factory.logoIV
    lazy private var launchLbl = factory.launchLbl
    lazy private var launchView = factory.view
    
    private var factory: LaunchFactory
    weak var coordinator: LaunchCoordinator?
    
    init(_ factory: LaunchFactory){
        self.factory = factory
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(logoLaunch)
        view.addSubview(launchView)
        
        logoLaunch.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-120.0)
            make.left.right.equalToSuperview().inset(24.0)
        }
        
        launchView.snp.makeConstraints { make in
            make.top.equalTo(logoLaunch.snp.bottom).offset(8.0)
            make.centerX.equalToSuperview()
            make.left.equalTo(logoLaunch).offset(32.0)
            make.right.equalTo(logoLaunch).offset(-32.0)
            make.height.equalTo(44.0)
        }
        launchView.addSubview(launchLbl)
        launchLbl.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
         }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      //  setTransparentNavBar(false)
        beginAlphaAnimation()
    }
    
    func beginAlphaAnimation() {
        UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseOut, animations: {
            self.factory.logoIV.alpha = 1
        }) { (isTrue) in
            self.endAlphaAnimation()
        }
    }
    
    fileprivate func endAlphaAnimation() {
        UIView.animate(withDuration: 1, delay: 1, options: .curveEaseIn, animations: {
            self.factory.logoIV.alpha = 0
        }) { (isTrue) in
            self.coordinator?.finish()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
