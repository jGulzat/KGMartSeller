//
//  RegisterVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit

class RegisterVC: BaseVC {
    
    lazy private var nameLbl = factory.nameLbl
    lazy private var nameTXF = factory.nameTXF
    lazy private var surnameLbl = factory.surnameLbl
    lazy private var surnameTXF = factory.surnameTXF
    lazy private var mailLbl = factory.mailLbl
    lazy private var mailTXF = factory.mailTXF
    lazy private var passwordLbl = factory.passwordLbl
    lazy private var passwordTXF = factory.passwordTXF
    lazy private var confirmPasswordLbl = factory.confirmPasswordLbl
    lazy private var confirmPasswordTXF = factory.confirmPasswordTXF
    lazy private var companyNameLbl = factory.companyNameLbl
    lazy private var companyNameTXF = factory.companyNameTXF
    lazy private var companyAdressLbl = factory.companyAddresLbl
    lazy private var companyAdressTXF = factory.companyAddresTXF
    lazy private var opfLbl = factory.opfLbl
    lazy private var opfTXF = factory.opfTXF
    lazy private var countryLbl = factory.countryLbl
    lazy private var numberLbl = factory.numberLbl
    lazy private var phoneNumberView = factory.phoneNumberView
    lazy private var phoneNumberTF = factory.phoneNumberTF
    lazy private var registerBtn = factory.registerBtn
    lazy private var registerLbl = factory.registerLbl
    
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    
    private var factory: RegisterFactory
    
    init(_ factory: RegisterFactory){
        self.factory = factory
        super.init()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.register
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.snp.makeConstraints { (make) in
            make.top.right.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-24.0)
        }
        contentView.snp.makeConstraints { make in
            make.top.right.bottom.left.equalToSuperview()
            make.height.equalTo(910.0)
            make.width.equalToSuperview()
        }
        
        contentView.addSubview(nameLbl)
        contentView.addSubview(nameTXF)
        contentView.addSubview(surnameLbl)
        contentView.addSubview(surnameTXF)
        contentView.addSubview(mailLbl)
        contentView.addSubview(mailTXF)
        contentView.addSubview(passwordLbl)
        contentView.addSubview(passwordTXF)
        contentView.addSubview(confirmPasswordLbl)
        contentView.addSubview(confirmPasswordTXF)
        contentView.addSubview(companyNameLbl)
        contentView.addSubview(companyNameTXF)
        contentView.addSubview(companyAdressLbl)
        contentView.addSubview(companyAdressTXF)
        contentView.addSubview(opfLbl)
        contentView.addSubview(opfTXF)
        contentView.addSubview(countryLbl)
        contentView.addSubview(phoneNumberView)
        contentView.addSubview(numberLbl)
      //  contentView.addSubview(phoneNumberTF)
        contentView.addSubview(registerBtn)
        contentView.addSubview(registerLbl)
        
        registerBtn.addTarget(self, action: #selector(registerUser), for: .touchUpInside)
        
        nameLbl.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        nameTXF.snp.makeConstraints { make in
            make.top.equalTo(nameLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        surnameLbl.snp.makeConstraints { make in
            make.top.equalTo(nameTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        surnameTXF.snp.makeConstraints { make in
            make.top.equalTo(surnameLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        mailLbl.snp.makeConstraints { make in
            make.top.equalTo(surnameTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        mailTXF.snp.makeConstraints { make in
            make.top.equalTo(mailLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        passwordLbl.snp.makeConstraints { make in
            make.top.equalTo(mailTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        passwordTXF.snp.makeConstraints { make in
            make.top.equalTo(passwordLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        confirmPasswordLbl.snp.makeConstraints { make in
            make.top.equalTo(passwordTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        confirmPasswordTXF.snp.makeConstraints { make in
            make.top.equalTo(confirmPasswordLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        companyNameLbl.snp.makeConstraints { make in
            make.top.equalTo(confirmPasswordTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        companyNameTXF.snp.makeConstraints { make in
            make.top.equalTo(companyNameLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        companyAdressLbl.snp.makeConstraints { make in
            make.top.equalTo(companyNameTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        companyAdressTXF.snp.makeConstraints { make in
            make.top.equalTo(companyAdressLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        opfLbl.snp.makeConstraints { make in
            make.top.equalTo(companyAdressTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        opfTXF.snp.makeConstraints { make in
            make.top.equalTo(opfLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        numberLbl.snp.makeConstraints { make in
            make.top.equalTo(opfTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        phoneNumberView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview().inset(16.0)
            make.top.equalTo(numberLbl.snp.bottom)
            make.height.equalTo(44.0)
        }
        phoneNumberView.addSubview(phoneNumberTF)
       // phoneNumberTF.addTarget(self, action: #selector(didChangePhoneValue), for: .editingChanged)
        phoneNumberTF.snp.remakeConstraints { make in
            make.left.right.top.bottom.equalToSuperview().inset(8.0)
        }
        registerBtn.snp.makeConstraints { make in
            make.top.equalTo(phoneNumberView.snp.bottom).offset(16.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        registerLbl.snp.makeConstraints { make in
            make.top.equalTo(registerBtn.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(20.0)
            make.bottom.equalToSuperview().offset(-24.0)
        }
    }
    
    @objc func registerUser(sender: UIButton){
       /* guard let name = nameTXF.text,
              let surname = surnameTXF.text,
              let mail = mailTXF.text,
              let password = passwordTXF.text,
              let confirmPassword = confirmPasswordTXF.text,
              let companyName = companyNameTXF.text,
              let companyAddress = companyAdressTXF.text,
              let opf */
    }
 }
