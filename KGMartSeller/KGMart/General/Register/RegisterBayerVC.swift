//
//  RegisterBayerVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 26/2/22.
//

import UIKit

class RegisterBayerVC: BaseVC {
    
    lazy private var nameLbl = factory.nameLbl
    lazy private var nameTXF = factory.nameTXF
    lazy private var surnameLbl = factory.surnameLbl
    lazy private var surnameTXF = factory.surnameTXF
    lazy private var mailLbl = factory.mailLbl
    lazy private var mailTXF = factory.mailTXF
    lazy private var passwordLbl = factory.passwordLbl
    lazy private var passwordTXF = factory.passwordTXF
    lazy private var confirmPasswordLbl = factory.confirmPasswordLbl
    lazy private var confirmPasswordTXF = factory.confirmPasswordTXF
    lazy private var passportLbl = factory.passportLbl
    lazy private var passportTXF = factory.passportTXF
    lazy private var kemVidanLbl = factory.kemVidanLbl
    lazy private var kemVidanTXF = factory.kemVidanTXF
    lazy private var datePassportLbl = factory.datePassportLbl
    lazy private var datePassportTXF = factory.datePasswordTXF
    lazy private var photoPassportLbl = factory.photoPassportLbl
    lazy private var photoFontIV = factory.photoFontIV
    lazy private var photoBackIV = factory.photoBackIV
    
    lazy private var countryLbl = factory.countryLbl
    lazy private var numberLbl = factory.numberLbl
    lazy private var phoneNumberView = factory.phoneNumberView
    lazy private var phoneNumberTF = factory.phoneNumberTF
    lazy private var registerBtn = factory.registerBtn
    lazy private var registerLbl = factory.registerLbl
    
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    
    private var factory: RegisterFactory
    
    init(_ factory: RegisterFactory){
        self.factory = factory
        super.init()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Регистрация"
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalToSuperview()
        }
        contentView.snp.makeConstraints { make in
            make.top.right.bottom.left.equalToSuperview()
            make.height.equalTo(950.0)
            make.width.equalToSuperview()
        }
        
        contentView.addSubview(nameLbl)
        contentView.addSubview(nameTXF)
        contentView.addSubview(surnameLbl)
        contentView.addSubview(surnameTXF)
        contentView.addSubview(mailLbl)
        contentView.addSubview(mailTXF)
        contentView.addSubview(passwordLbl)
        contentView.addSubview(passwordTXF)
        contentView.addSubview(confirmPasswordLbl)
        contentView.addSubview(confirmPasswordTXF)
        contentView.addSubview(passportLbl)
        contentView.addSubview(passportTXF)
        contentView.addSubview(kemVidanLbl)
        contentView.addSubview(kemVidanTXF)
        contentView.addSubview(datePassportLbl)
        contentView.addSubview(datePassportTXF)
        contentView.addSubview(photoPassportLbl)
        contentView.addSubview(phoneNumberView)
        contentView.addSubview(numberLbl)
      //  contentView.addSubview(phoneNumberTF)
        contentView.addSubview(registerBtn)
        contentView.addSubview(registerLbl)
        
        
        nameLbl.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        nameTXF.snp.makeConstraints { make in
            make.top.equalTo(nameLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        surnameLbl.snp.makeConstraints { make in
            make.top.equalTo(nameTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        surnameTXF.snp.makeConstraints { make in
            make.top.equalTo(surnameLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        mailLbl.snp.makeConstraints { make in
            make.top.equalTo(surnameTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        mailTXF.snp.makeConstraints { make in
            make.top.equalTo(mailLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        passwordLbl.snp.makeConstraints { make in
            make.top.equalTo(mailTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        passwordTXF.snp.makeConstraints { make in
            make.top.equalTo(passwordLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        confirmPasswordLbl.snp.makeConstraints { make in
            make.top.equalTo(passwordTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        confirmPasswordTXF.snp.makeConstraints { make in
            make.top.equalTo(confirmPasswordLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        passportLbl.snp.makeConstraints { make in
            make.top.equalTo(confirmPasswordTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        passportTXF.snp.makeConstraints { make in
            make.top.equalTo(passportLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        kemVidanLbl.snp.makeConstraints { make in
            make.top.equalTo(passportTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        kemVidanTXF.snp.makeConstraints { make in
            make.top.equalTo(kemVidanLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        photoPassportLbl.snp.makeConstraints { make in
            make.top.equalTo(kemVidanTXF.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        let photoView = UIView()
        contentView.addSubview(photoView)
        photoView.snp.makeConstraints { make in
            make.top.equalTo(photoPassportLbl.snp.bottom)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(100.0)
        }
        photoView.addSubview(photoFontIV)
        photoView.addSubview(photoBackIV)
        photoFontIV.snp.makeConstraints { make in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(photoBackIV.snp.left)
           // make.width.equalTo(100.0)
        }
        photoBackIV.snp.makeConstraints { make in
            make.right.top.bottom.equalToSuperview()
            make.left.equalTo(photoFontIV.snp.right).offset(8.0)
            make.width.equalTo(photoFontIV)
        }
        numberLbl.snp.makeConstraints { make in
            make.top.equalTo(photoView.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(32.0)
        }
        phoneNumberView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview().inset(16.0)
            make.top.equalTo(numberLbl.snp.bottom)
            make.height.equalTo(44.0)
        }
        phoneNumberView.addSubview(phoneNumberTF)
       // phoneNumberTF.addTarget(self, action: #selector(didChangePhoneValue), for: .editingChanged)
        phoneNumberTF.snp.remakeConstraints { make in
            make.left.right.top.bottom.equalToSuperview().inset(8.0)
        }
        registerBtn.snp.makeConstraints { make in
            make.top.equalTo(phoneNumberView.snp.bottom).offset(16.0)
            make.left.right.equalToSuperview().inset(16.0)
            make.height.equalTo(44.0)
        }
        registerLbl.snp.makeConstraints { make in
            make.top.equalTo(registerBtn.snp.bottom).offset(8.0)
            make.left.right.equalToSuperview().inset(20.0)
            make.bottom.equalToSuperview().offset(-24.0)
        }
    }
}
