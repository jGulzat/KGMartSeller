//
//  LoginVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit
import SnapKit

class LoginVC: BaseVC, UINavigationControllerDelegate {
    
    lazy private var logoIV = factory.logoIV
    lazy private var loginTXF = factory.loginTxf
    lazy private var passwordTXF = factory.passwordTxf
    lazy private var forgotPassBtn = factory.forgotPassBtn
    lazy private var loginBtn = factory.loginBtn
    lazy private var accountLbl = factory.accountLbl
    lazy private var registerBtn = factory.registerBtn
    
    private var factory: LoginFactory
    weak var coordinator: LoginCoordinator?
    
    init(_ factory: LoginFactory){
        self.factory = factory
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(logoIV)
        view.addSubview(loginTXF)
        view.addSubview(passwordTXF)
        view.addSubview(forgotPassBtn)
        view.addSubview(loginBtn)
        view.addSubview(accountLbl)
        view.addSubview(registerBtn)
        
        registerBtn.addTarget(self, action: #selector(goToSheduler), for: .touchUpInside)
        loginBtn.addTarget(self, action: #selector(loginUser), for: .touchUpInside)
        
        passwordTXF.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(24.0)
            make.height.equalTo(44.0)
        }
        
        loginTXF.snp.makeConstraints { make in
            make.bottom.equalTo(passwordTXF.snp.top).offset(-16.0)
            make.leading.trailing.equalToSuperview().inset(24.0)
            make.height.equalTo(44.0)
        }
        
        logoIV.snp.makeConstraints { make in
            make.width.equalTo(250.0)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(loginTXF.snp.top).offset(-24.0)
         }
        
        forgotPassBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordTXF.snp.bottom).offset(8.0)
            make.trailing.equalTo(passwordTXF.snp.trailing)
            make.height.equalTo(32.0)
        }
        loginBtn.snp.makeConstraints { make in
            make.top.equalTo(forgotPassBtn.snp.bottom).offset(8.0)
            make.leading.trailing.equalToSuperview().inset(24.0)
            make.height.equalTo(44.0)
        }
        
        registerBtn.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-16.0)
            make.centerX.equalToSuperview()
            make.height.equalTo(44.0)
        }
        accountLbl.snp.makeConstraints { make in
            make.bottom.equalTo(registerBtn.snp.top)
            make.centerX.equalToSuperview()
        }
    }
    
    @objc func goToSheduler(sender: UIButton){
        
        self.navigationController?.pushViewController(ShedulerVC(.init()), animated: true)
    }
    
    @objc func loginUser(sender: UIButton){
        
        let login = "KGMart"
        let password = "KGMart2022"
        print("login: \(login), password: \(password)")
        guard login == loginTXF.text, password == passwordTXF.text else { return }
        
        let alert = UIAlertController(title: "Login", message: "Login succesfully",
                                      preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
