//
//  LoginFactory.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit

class LoginFactory {
    lazy var logoIV = UIImageView().then {
        $0.image = UIImage(named: "logo_launch")
        $0.backgroundColor = .clear
        $0.contentMode = .scaleAspectFill
    }
    
    lazy var loginTxf = UITextField().then {
        $0.placeholder = "Почта или логин"
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 32, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
    }
    lazy var passwordTxf = UITextField().then {
        $0.placeholder = "Пароль"
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 32, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var forgotPassBtn = UIButton().then {
        $0.backgroundColor = .white
        $0.setTitle("Забыли пароль?", for: .normal)
        $0.setTitleColor(UIColor(red: 136/255, green: 136/255, blue: 136/255, alpha: 1), for: .normal)
    }
    lazy var loginBtn = UIButton().then {
        $0.backgroundColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.setTitle("Войти", for: .normal)
        $0.setTitleColor(.white, for: .normal)
        $0.layer.cornerRadius = 10.0
    }
    lazy var accountLbl = UILabel().then {
        $0.backgroundColor = .white
        $0.text = "Нет аккаунта?"
        $0.textColor = UIColor(red: 136/255, green: 136/255, blue: 136/255, alpha: 1)
        $0.font = UIFont(name: "", size: 14)
        $0.textAlignment = .center
    }
    lazy var registerBtn = UIButton().then {
        $0.backgroundColor = .white
        $0.setTitleColor(UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1), for: .normal)
        $0.setTitle("Зарегистрируйтесь", for: .normal)
    }
}
