//
//  RegisterFactory.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit
import PhoneNumberKit

class RegisterFactory {
    
    lazy var nameLbl = UILabel().then {
        $0.text = "Имя"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    
    lazy var nameTXF = UITextField().then {
        $0.placeholder = "Имя"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
    }
    lazy var surnameLbl = UILabel().then {
        $0.text = "Фамилия"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    
    lazy var surnameTXF = UITextField().then {
        $0.placeholder = "Фамилия"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
    }
    lazy var mailLbl = UILabel().then {
        $0.text = "E-mail"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    
    lazy var mailTXF = UITextField().then {
        $0.placeholder = "E-mail"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
    }
    lazy var passwordLbl = UILabel().then {
        $0.text = "Пароль (от 4 символов)"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    
    lazy var passwordTXF = UITextField().then {
        $0.placeholder = "Пароль (от 4 символов)"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var confirmPasswordLbl = UILabel().then {
        $0.text = "Подтвердите пароль"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var confirmPasswordTXF = UITextField().then {
        $0.placeholder = "Подтвердите пароль"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var companyNameLbl = UILabel().then {
        $0.text = "Название компании"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var companyNameTXF = UITextField().then {
        $0.placeholder = "Название компании"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var companyAddresLbl = UILabel().then {
        $0.text = "Адрес компании или (контейнера)"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var  companyAddresTXF = UITextField().then {
        $0.placeholder = "Адрес компании или (контейнера)"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var opfLbl = UILabel().then {
        $0.text = "ОПФ: ИП, ООО и т.д."
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var opfTXF = UITextField().then {
        $0.placeholder = "ОПФ: ИП, ООО и т.д."
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    
    lazy var phoneNumberView: UIView = UIView().then {
        $0.layer.cornerRadius = 10.0
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 8
        $0.layer.borderColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1).cgColor
        $0.layer.borderWidth = 1
    //    $0.backgroundColor = Asset.componentBackColor.color
      //  $0.shadowToBottom()
    }
    
    lazy var phoneNumberTF: PhoneNumberTextField = PhoneNumberTextField().then {
        $0.withDefaultPickerUI = true
        $0.withPrefix = true
        $0.withFlag   = true
        $0.withExamplePlaceholder = true
        $0.smartInsertDeleteType  = .yes
      //  $0.textColor = Asset.textPrimary.color
        $0.font = UIFont(name: "", size: 16)
    }
    lazy var countryLbl: UILabel = UILabel().then{
        $0.text = "Страна:"
        $0.font = UIFont(name: "", size: 16.0)
        $0.textAlignment = .left
    }
    lazy var numberLbl: UILabel = UILabel().then {
        $0.text = "Номер"
        $0.font = UIFont(name: "", size: 16.0)
        $0.textAlignment = .left
    }
    lazy var registerBtn: UIButton = UIButton().then {
        $0.backgroundColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.setTitle("Зарегистрироваться", for: .normal)
        $0.setTitleColor(.white, for: .normal)
        $0.layer.cornerRadius = 10.0
    }
    lazy var registerLbl: UILabel = UILabel().then{
        $0.text = "Регистрируясь, вы соглашаетесь с условиями пользовательского соглашения"
        $0.textColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.textAlignment = .center
        $0.lineBreakMode = .byWordWrapping
        $0.numberOfLines = 0
    }
    //bayer
    lazy var passportLbl = UILabel().then {
        $0.text = "ID паспорта"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var passportTXF = UITextField().then {
        $0.placeholder = "ID паспорта"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var kemVidanLbl = UILabel().then {
        $0.text = "Кем выдан"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var kemVidanTXF = UITextField().then {
        $0.placeholder = "Кем выдан"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }

    lazy var datePassportLbl = UILabel().then {
        $0.text = "Дата выдачи"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var datePasswordTXF = UITextField().then {
        $0.placeholder = "Дата выдачи"
        $0.textColor = UIColor(red: 27/255, green: 27/255, blue: 27/255, alpha: 1)
        $0.font = UIFont(name: "", size: 16)
        $0.layer.borderWidth = 1.0
        $0.clearButtonMode = .always
        $0.layer.cornerRadius = 10.0
        let leftView = UIView(frame: .init(x: 0, y: 0, width: 24, height: 32))
        $0.leftViewMode = .always
        $0.leftView = leftView
        $0.isSecureTextEntry = true
    }
    lazy var photoPassportLbl = UILabel().then {
        $0.text = "Фото паспорта"
        $0.font = UIFont(name: "", size: 16)
        $0.textAlignment = .left
    }
    lazy var photoFontIV = UIImageView().then {
        $0.layer.cornerRadius = 8
        $0.image = UIImage(named: "ic_camera")
    }
    lazy var photoBackIV = UIImageView().then {
        $0.layer.cornerRadius = 8
        $0.image = UIImage(named: "ic_camera")
    }



}
