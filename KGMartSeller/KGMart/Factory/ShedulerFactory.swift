//
//  ShedulerFactory.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit

class ShedulerFactory{
    
    lazy var fabricatorBtn = UIButton().then {
        $0.backgroundColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.setTitle("Производитель", for: .normal)
        $0.setTitleColor(.white, for: .normal)
        $0.layer.cornerRadius = 8.0
    }
    
    lazy var bayerBtn = UIButton().then {
        $0.backgroundColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.setTitle("Вайер", for: .normal)
        $0.setTitleColor(.white, for: .normal)
        $0.layer.cornerRadius = 8.0
    }
}
