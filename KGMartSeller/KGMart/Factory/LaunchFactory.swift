//
//  LaunchFactory.swift
//  KGMartSeller
//
//  Created by Guzlat on 7/3/22.
//

import UIKit

class LaunchFactory {
    lazy var logoIV = UIImageView().then {
        $0.image = UIImage(named: "logo_launch")
        $0.backgroundColor = .clear
        $0.contentMode = .scaleAspectFill
    }
    
    lazy var view = UIView().then {
        $0.backgroundColor = UIColor(red: 110/255, green: 47/255, blue: 207/255, alpha: 1)
        $0.layer.cornerRadius = 10.0
    }
    
    lazy var launchLbl = UILabel().then{
        $0.text = "Партнёры"
       // $0.font = UIFont.systemFont(ofSize: 16.0)
        $0.font = UIFont.boldSystemFont(ofSize: 16.0)
        $0.textColor = .white
        $0.textAlignment = .center
    }
}
