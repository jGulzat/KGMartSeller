//
//  Coordinator.swift
//  KGMartSeller
//
//  Created by Guzlat on 7/3/22.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators   : [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()

    func openLaunch()
}


extension Coordinator {
    
    func openLaunch() {
        let launchCoordinator = LaunchCoordinator(navigationController)
        childCoordinators.append(launchCoordinator)
        launchCoordinator.start()
    }
    
}


