//
//  LaunchCoordinator.swift
//  KGMartSeller
//
//  Created by Guzlat on 10/3/22.
//

import UIKit

class LaunchCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    
    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        navigationController.delegate = self
        let launchVC = LaunchVC(.init())
        launchVC.coordinator = self
        
        let nav = UINavigationController(rootViewController: launchVC)
        navigationController = nav
        UIApplication.keyWindow?.rootViewController = navigationController
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    func finish() {
        let loginCoordinator = LoginCoordinator(navigationController)
        childCoordinators.append(loginCoordinator)
        loginCoordinator.start()
    }
}
