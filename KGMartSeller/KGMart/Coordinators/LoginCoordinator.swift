//
//  LoginCoordinator.swift
//  KGMartSeller
//
//  Created by Guzlat on 11/3/22.
//

import UIKit

final class LoginCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {

    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    var currentVC : LoginVC
    
    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.currentVC = LoginVC(.init())
    }
    
    func start() {
        navigationController.delegate = self
        currentVC.coordinator = self
        
        let nav = UINavigationController(rootViewController: currentVC)
        nav.modalPresentationStyle = .formSheet
        navigationController.present(nav, animated: true) { [weak self] in
            guard let self = self else { return }
            self.navigationController = nav
        }
    }
    
    func finish() {
        navigationController.popViewController(animated: true)
    }
}
