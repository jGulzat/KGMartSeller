//
//  BaseVC.swift
//  KGMartSeller
//
//  Created by Guzlat on 23/2/22.
//

import UIKit

class BaseVC: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        print("Init \(String(describing: self))")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }
}
