//
//  UIApplication+Extensions.swift
//  KGMartSeller
//
//  Created by Guzlat on 10/3/22.
//

import UIKit

extension UIApplication {
    
    static var keyWindow : UIWindow? {
        return UIApplication.shared.connectedScenes
            .first(where: { $0 is UIWindowScene })
            .flatMap({ $0 as? UIWindowScene })?.windows
            .first(where: \.isKeyWindow)
    }
    
}
